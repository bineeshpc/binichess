from engine import Engine

def test_get_move():
    pos ='position startpos moves g2g4 e7e6 f2f3\ngo movetime 1000\n'
    e = Engine()
    e.getmove(pos)
    while True:
        if e.bestmove:
            assert e.bestmove == 'd8h4'
            break

def test_promotion_move():
    pos ='position startpos moves e2e4 e7e5 g1f3 g8f6 f3e5 d7d6 e5f3 f6e4 f1c4'
    pos += ' d6d5 c4d3 e4c5 d3e2 d5d4 f3e5 d8e7 f2f4 f7f6 e5f3 d4d3'
    # pos += ' e1g1 d3e2 d1e1\ngo movetime 1000\n'

    pos += ' e1g1 d3e2 d1e1 e2f1q\ngo movetime 1000\n'
    e = Engine()
    e.getmove(pos)
    while True:
        if e.bestmove:
            print(e.bestmove)
            break


def test_promotion_move_one():
    pos = 'position startpos moves e2e4 c7c5 g1f3 b8c6 f1b5 c6d4 f3d4 c5d4 c2c3 '
    pos += 'e7e5 e1g1 a7a6 b5c4 b7b5 c4d5 a8b8 f2f4 d7d6 f4e5 d6e5 d5f7 e8e7 f7d5 '
    pos += 'g8f6 d1f3 c8e6 d5e6 e7e6 d2d3 f8c5 g1h1 h8f8 c1g5 b8b6 b1d2 e6f7 d2b3 '
    pos += 'c5e7 c3d4 f7g8 f3h3 f8f7 d4e5 f6e4 g5e7 d8e7 h3c8 f7f8 f1f8 e7f8 c8f8 '
    pos += 'g8f8 d3e4 f8e7 a1d1 b6e6 d1d5 e6c6 h2h3 g7g5 h1h2 h7h5 b3d4 c6c4 h2g3 '
    pos += 'b5b4 g3f3 a6a5 d4f5 e7e6 d5a5 c4c2 b2b3 c2c3 f3f2 c3c2 f2g3 c2c3 g3h2 '
    pos += 'c3c2 f5d4 e6d7 d4c2 d7c6 c2d4 c6b6 e5e6 b6a5 e6e7 a5b6 e7e8q b6c5 d4e6 '
    pos += 'c5d6\ngo movetime 1000\n'

    e = Engine()
    e.getmove(pos)
    while True:
        if e.bestmove:
            print(e.bestmove)
            break

