from  bclogger import logging
from square import Square
from position import Position

import re
class Move:
    """Move can be short algebraic form"""
    def __init__(self, halfmove, position):
        if isinstance(halfmove, str):
            
            self.piece = halfmove[0]
            if self.piece.islower():
                self.piece = 'P'
            if self.piece == 'O':
                self.piece = 'K'
            logging.debug(self.piece)
            #print self.piece
            m = re.search(r'[a-h][1-8]|O-O-O|O-O', halfmove)
            if m is not None:
                movestring = m.group()
                self.target = self.findtargetsquare(self.piece, position.sidetomove, movestring)
                m = re.search(r'(?P<disambiguation>[a-h]|[1-8]|[a-h]x|[1-9]x)[a-h][1-8]', halfmove)
                if m is not None:
                    self.disambiguation = m.groupdict()['disambiguation'].strip('x')
                else: self.disambiguation = ''
                logging.debug(self.target)
                m = re.search(r'=(?P<promotion>Q|N|R|B)', halfmove)
                if m is not None:
                    self.promotion = m.groupdict()['promotion']
                else: self.promotion = ''
                self.position = position
                if(position.sidetomove == 'b'):
                    self.piece = self.piece.lower()
                
                #print self.piece
                #self.position.display()
                self.source = self.findsourcesquare(self.piece, position.sidetomove,
                                            self.disambiguation)
                #print "source, target,promotion"+str(self.source)+str(self.target)+self.promotion
                logging.debug(str(self.source)+str(self.target)+self.promotion)
                self.move = str(self.source) + str(self.target) + self.promotion
                self.newposition = self.get_newposition(position, movestring)
                #self.newposition.display()

    def get_newposition(self, position, movestring):
        try:
            src_index = self.source.squarenum
            tgt_index = self.target.squarenum
        except AttributeError:
            print "Either source square or targetsquare not found"
            print position, movestring
            return "Either source square or targetsquare not found"
        position.position[src_index].content = ''
        position.position[tgt_index].content = self.promotion or self.piece
        #print movestring,  "newposition"
        if movestring == 'O-O-O':
            if position.sidetomove == 'w':
                position.position[Square('a1').squarenum].content = ''
                position.position[Square('d1').squarenum].content = 'R'
            else:
                position.position[Square('a8').squarenum].content = ''
                position.position[Square('d8').squarenum].content = 'r'
        elif movestring == 'O-O':
            if position.sidetomove == 'w':
                position.position[Square('h1').squarenum].content = ''
                position.position[Square('f1').squarenum].content = 'R'
            else:
                position.position[Square('h8').squarenum].content = ''
                position.position[Square('f8').squarenum].content = 'r'
                
        if position.sidetomove == 'w':
            position.sidetomove = 'b'
        else:
            position.sidetomove = 'w'
        return position

    def findtargetsquare(self, piece, sidetomove, movestring):

        if movestring == 'O-O':
            if sidetomove == 'w': movestring = 'g1'
            else:movestring = 'g8'
        elif movestring == 'O-O-O':
            if sidetomove == 'w': movestring = 'c1'
            else:movestring = 'c8'            
        return Square(movestring)
        
    def findsourcesquare(self, piece, sidetomove, disambiguation=''):
        func = {'K':self.king, 'Q':self.queen, 'B':self.bishop,
                'N':self.knight, 'R':self.rook, 'P':self.pawn}

        #logging.debug(piece, sidetomove, disambiguation)
        for file, rank in func[piece.upper()](self.target, sidetomove):
            #print rank, file, rank * 8 + file
            piece = self.position.position[rank * 8 + file].content

            #print piece, self.piece, disambiguation
            if piece == self.piece and disambiguation is '':
                return Square((file, rank))
            else:
                try:
                    dis = int(disambiguation) - 1
##                    print piece, self.piece, disambiguation, file, rank
                    if piece == self.piece and rank == dis:
                        return Square((file, rank))
                except ValueError:
##                    print "value error"
##                    print piece, self.piece, disambiguation, file, rank
                    if piece == self.piece and file == ord(disambiguation) - ord('a'):
                        return Square((file, rank))
                
        
        

    def king(self, square, colour): 
        file, rank = square.coordinates
        normalmove = [(x, y) for x in range(file - 1, file + 2)
                for y in range(rank - 1 , rank + 2) if (x, y) != (file, rank) and x >= 0 and y>=0 and x <= 7 and y <= 7]
        if colour == 'w':
            castlemove = [(4, 0)]
        else:
            castlemove = [(4, 7)]
        return normalmove + castlemove

    def pawn(self, square, colour):
        file, rank = square.coordinates
        if colour == 'w':
            return [i for i in [(file, rank - 1), (file, rank - 2),
                                    (file + 1, rank - 1), (file - 1, rank -1)]]
        else:
            return [i for i in [(file, rank + 1), (file, rank + 2),
                                    (file + 1, rank + 1), (file - 1, rank +1)]]
            

    def queen(self, square, colour):
        '''Rook like move'''
        file, rank = square.coordinates
        if colour == 'b':
            piece = 'q'
        else:
            piece = 'Q'
        sq=[]
        for y in range(rank - 1, -1, -1):
            s = (file, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        for y in range(rank + 1, 8):
            s = (file, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        for x in range(file - 1, -1, -1):
            s = (x, rank)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        for x in range(file + 1, 8):
            s = (x, rank)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)

        '''Bishop like move'''
        x, y = file, rank
        while(x <=6 and y <= 6):
            x += 1
            y += 1
            s = (x, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        x, y = file, rank
        while(x >= 1 and y >=1):
            x -= 1
            y -= 1
            s = (x, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        x, y = file, rank
        while(x >= 1 and y <=6):
            x -= 1
            y += 1
            s = (x, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        x, y = file, rank
        while(x <=6 and y >=1):
            x += 1
            y -= 1
            s = (x, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)           
        return sq

    def rook(self, square, colour):
        file, rank = square.coordinates
        if colour == 'b':
            piece = 'r'
        else:
            piece = 'R'
        sq=[]
        for y in range(rank - 1, -1, -1):
            s = (file, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        for y in range(rank + 1, 8):
            s = (file, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        for x in range(file - 1, -1, -1):
            s = (x, rank)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        for x in range(file + 1, 8):
            s = (x, rank)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        return sq
        #return [(x, rank) for x in range(8)] + [(file, y) for y in range(8)]

    def knight(self, square, colour):
        file, rank = square.coordinates
        sq = list(set([(file + 2, rank + 1), (file + 1, rank - 2), (file + 2, rank - 1), (file + 1, rank + 2),
              (file - 2, rank + 1), (file - 1, rank + 2), (file - 2, rank - 1), (file - 1, rank - 2)]))
        sq = [(x, y) for x, y in sq if x >= 0 and y>=0 and x <= 7 and y <= 7] 
        return sq
        

    def bishop(self, square, colour):
        file, rank = square.coordinates

        if colour == 'b':
            piece = 'b'
        else:
            piece = 'B'
        sq = []
        x, y = file, rank
        while(x <=6 and y <= 6):
            x += 1
            y += 1
            s = (x, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        x, y = file, rank
        while(x >= 1 and y >=1):
            x -= 1
            y -= 1
            s = (x, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        x, y = file, rank
        while(x >= 1 and y <=6):
            x -= 1
            y += 1
            s = (x, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)
        x, y = file, rank
        while(x <=6 and y >=1):
            x += 1
            y -= 1
            s = (x, y)
            content = self.position.position[Square(s).squarenum].content
            if content != '' and content != piece: break
            sq.append(s)           
        return sq
        

    def __str__(self):
        return self.move

    def __repr__(self):
        return self.move
