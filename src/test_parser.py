from parser import Parser

def test_seek():
    a = """

GuestCLYD (++++) seeking 10 0 unrated blitz ("play 26" to respond)

fics% """
    p = Parser()
    outdict = {u'seconds': '0', u'player': 'GuestCLYD', u'minutes': '10', u'gamenumber': '26'}
    assert p.get_matched_pattern(a, 'seek game') == outdict


def test_seek_failure():
    a = """GuestCLYD"""
    p = Parser()
    assert p.get_matched_pattern(a, 'seek game') == None

def test_pattern_with_moves():
    a = """

Game 92 (rebkley vs. GuestNLOK)


       ---------------------------------

    1  |   | K | R |   |   |   |   |   |     Move # : 28 (Black)

       |---+---+---+---+---+---+---+---|

    2  |   | P |   |   |   |   | P | P |     White Moves : 'Kg1     (0:00)'

       |---+---+---+---+---+---+---+---|

    3  |   |   |   |   |   | P |   |   |

       |---+---+---+---+---+---+---+---|

    4  | *Q|   |   | *R|   |   |   |   |     Black Clock : 2:54

       |---+---+---+---+---+---+---+---|

    5  | *B|   |   | *B| *P|   |   |   |     White Clock : 2:06

       |---+---+---+---+---+---+---+---|

    6  |   |   |   | *K|   |   |   |   |     Black Strength : 23

       |---+---+---+---+---+---+---+---|

    7  |   | Q |   |   |   |   | *P| *P|     White Strength : 18

       |---+---+---+---+---+---+---+---|

    8  |   |   |   |   |   |   |   |   |

       ---------------------------------

         h   g   f   e   d   c   b   a

fics% """
    
    p = Parser()
    outdict = {u'movenumber': '28', u'sidetomove': 'Black',
     u'move': 'Kg1', u'timeused': '0:00',
      u'whiteclock': '2:06', u'game': '92',
       u'black': 'GuestNLOK', u'whitestrength': '18',
        u'blackclock': '2:54', u'white': 'rebkley', u'blackstrength': '23'}
    assert p.get_pattern_with_moves(a) == outdict


def test():
    a = """

Game 92 (rebkley vs. GuestNLOK)


       ---------------------------------

    1  | R | N | B | K | Q | B | N | R |     Move # : 1 (White)

       |---+---+---+---+---+---+---+---|

    2  | P | P | P | P | P | P | P | P |

       |---+---+---+---+---+---+---+---|

    3  |   |   |   |   |   |   |   |   |

       |---+---+---+---+---+---+---+---|

    4  |   |   |   |   |   |   |   |   |     Black Clock : 5:00

       |---+---+---+---+---+---+---+---|

    5  |   |   |   |   |   |   |   |   |     White Clock : 5:00

       |---+---+---+---+---+---+---+---|

    6  |   |   |   |   |   |   |   |   |     Black Strength : 39

       |---+---+---+---+---+---+---+---|

    7  | *P| *P| *P| *P| *P| *P| *P| *P|     White Strength : 39

       |---+---+---+---+---+---+---+---|

    8  | *R| *N| *B| *K| *Q| *B| *N| *R|

       ---------------------------------

         h   g   f   e   d   c   b   a


Game 92: A disconnection will be considered a forfeit.

fics%"""

    p = Parser()
    outdict = {u'movenumber': '1', u'sidetomove': 'White', u'whiteclock': '5:00', u'game': '92', u'black': 'GuestNLOK', u'whitestrength': '39', u'blackclock': '5:00', u'white': 'rebkley', u'blackstrength': '39'}
    print(p.get_pattern_without_moves(a))
    assert p.get_pattern_without_moves(a) == outdict


def test_is_pattern_match_pass():
    a = """

Game 92 (rebkley vs. GuestNLOK)


       ---------------------------------

    1  | R | N | B | K | Q | B | N | R |     Move # : 1 (White)

       |---+---+---+---+---+---+---+---|

    2  | P | P | P | P | P | P | P | P |

       |---+---+---+---+---+---+---+---|

    3  |   |   |   |   |   |   |   |   |

       |---+---+---+---+---+---+---+---|

    4  |   |   |   |   |   |   |   |   |     Black Clock : 5:00

       |---+---+---+---+---+---+---+---|

    5  |   |   |   |   |   |   |   |   |     White Clock : 5:00

       |---+---+---+---+---+---+---+---|

    6  |   |   |   |   |   |   |   |   |     Black Strength : 39

       |---+---+---+---+---+---+---+---|

    7  | *P| *P| *P| *P| *P| *P| *P| *P|     White Strength : 39

       |---+---+---+---+---+---+---+---|

    8  | *R| *N| *B| *K| *Q| *B| *N| *R|

       ---------------------------------

         h   g   f   e   d   c   b   a


Game 92: A disconnection will be considered a forfeit.

fics%"""
    p = Parser()
    assert p.is_pattern_match(a, 'pattern without moves') == True

def test_is_pattern_match_failure():
    a = """bla"""
    p = Parser()
    assert p.is_pattern_match(a, 'pattern without moves') == False
