import subprocess
import glob
import argparse
import os

def parse_cmdline():
    parser = argparse.ArgumentParser(description='test tools for binichess')
    parser.add_argument('--coverage', 
                        type=bool,
                        default=False,
                        help='Run coverage on all files')
    
    parser.add_argument('--coverage_pytest', 
                        type=bool,
                        default=False,
                        help='Run coverage pytest on all test files')

    parser.add_argument('--coverage_pytest_file', 
                        type=bool,
                        default=False,
                        help='Run coverage on a particular pytest file')


    parser.add_argument('--filename', 
                        type=str,
                        default=None,
                        help='pytest filename to run coverage on')


    parser.add_argument('--dont_suppress_stdout', 
                        type=bool,
                        default=False,
                        help='Pass this as true if the stdout of pytest filename should not be suppressed')


    parser.add_argument('--open_in_firefox', 
                        type=bool,
                        default=False,
                        help='Open all the coverage html files in firefox')

    args = parser.parse_args()
    return args


def get_output(cmd_lst):
    p1 = subprocess.Popen(cmd_lst, stdout=subprocess.PIPE)
    return p1.communicate()[0]

def system(cmd_list):
    cmd = ' '.join(cmd_list)
    status = subprocess.call(cmd, shell=True)
    return status

def coverage():
    python_files = glob.glob('*.py')
    python_files.remove('binichess2.py')
    python_files.remove('run_coverage.py')
    all_test_files = [i for i in python_files if 'test' in i]

    for python_file in all_test_files:
        cmd1_lst = ['coverage', 'run', python_file]
        print(get_output(cmd1_lst))
        cmd2_lst = ['coverage', 'html']
        print(get_output(cmd2_lst))
        print(cmd1_lst)

def coverage_pytest():
    cmd_lists = []
    cmd_lists.append(['coverage', 'run', '-m', 'pytest'])
    cmd_lists.append(['coverage', 'html'])
    for cmd_list in cmd_lists:
        system(cmd_list)


def coverage_pytest_file(filename, dont_suppress_stdout=True):
    cmd_lists = []
    if dont_suppress_stdout:
        cmd_lists.append(['coverage', 'run', '-m', 'pytest', '-s', filename])
    else:
        cmd_lists.append(['coverage', 'run', '-m', 'pytest', filename])
    cmd_lists.append(['coverage', 'html'])
    for cmd_list in cmd_lists:
        system(cmd_list)


def coverage_html_in_firefox():
    python_test_files = glob.glob('test*.py')
    cmd_lists = []
    for python_test_file in python_test_files:
        python_test_file = python_test_file.replace('.', '_')
        python_file = python_test_file.replace('test_', '')
        python_test_file_html = 'file:///home/bineesh/temp/binichess/src/htmlcov/{}.html'.format(python_test_file)
        python_file_html = 'file:///home/bineesh/temp/binichess/src/htmlcov/{}.html'.format(python_file)
        cmd_lists.append(['firefox', python_test_file_html])
        cmd_lists.append(['firefox', python_file_html])
    
    for cmd_list in cmd_lists:        
        system(cmd_list)


def main():
    args = parse_cmdline()
    if args.coverage:
        coverage()
    if args.coverage_pytest:
        coverage_pytest()
    if args.open_in_firefox:
        coverage_html_in_firefox()
    if args.coverage_pytest_file:
        coverage_pytest_file(args.filename, args.dont_suppress_stdout)
        

if __name__ == "__main__":
    main()