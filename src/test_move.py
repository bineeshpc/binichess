from move import Move
from position import Position
import pytest

def test_pawn_move():

    startpos = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
    assert str(Move('e4', Position(startpos))) == 'e2e4'

def test_rook_move():
    posforrook = "rnbqkbnr/1ppppppp/8/p7/7P/7R/PPPPPPP1/RNBQKBN1 b Qkq - 0 2"
    assert str(Move('Ra6', Position(posforrook))) == 'a8a6'

def test_bishop_move():
    posforbishop = "rnbqkbnr/pppp1ppp/8/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 0 2"
    assert str(Move('Bd6', Position(posforbishop))) == 'f8d6'

def test_queen_move():
    posforbishop = "rnbqkbnr/pppp1ppp/8/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 0 2"
    posforqueen = posforbishop
    assert str(Move('Qf6', Position(posforqueen))) == 'd8f6'

def test_knight_move():
    posforbishop = "rnbqkbnr/pppp1ppp/8/4p3/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 0 2"
    posforknight = posforbishop
    assert str(Move('Nf6', Position(posforknight))) == 'g8f6'

def test_king_move():
    posforking = 'r4rk1/6Rp/2p2qpP/1p1p4/pnnP4/2N3P1/PPP2P2/2K1Q2R b - - 0 23'
    assert str(Move('Kh8', Position(posforking))) == 'g8h8'
    posforking = 'rnbqk2r/ppp1bppp/5n2/3p2B1/3P4/2NQ4/PPP2PPP/R3KBNR b KQkq - 0 6'
    assert str(Move('O-O', Position(posforking))) == 'e8g8'

def test_rook_move1():
    posforrook = 'r2qkbnr/pppb1ppp/2npp3/8/4P3/5N2/PPPPBPPP/RNBQ1RK1 w kq - 0 5'
    assert str(Move('Re1', Position(posforrook))) == 'f1e1'

def test_pawn_move1():
    posforpawn = 'r1bqkb1r/p1p1pp1p/2pp1np1/8/3PP3/2N2N2/PPP2PPP/R1BQK2R b KQkq - 0 6'
    assert str(Move('c5', Position(posforpawn))) == 'c6c5'

def test_move_repr():
    posforrook = 'Q1N5/1rp3pp/4k3/5p2/8/4B3/2P2PPP/R3K2R b KQ - 0 1'
    assert repr(Move('Rb4', Position(posforrook))) == 'b7b4'

def test_different_positions():
    positions = [['4r1k1/r1n3p1/1pp2p2/7p/3P3K/2P5/1P2p1P1/8 b - - 0 1', 'e1=Q+', 'e2e1Q'],
                 ['4r1k1/r1n3p1/1pp2p2/7p/3P4/2P4K/1P2p1P1/8 b - - 0 1', 'e1=Q+', 'e2e1Q'],
                 ['4rr2/pp4k1/2ppnp2/5R2/3bP2Q/1P1P3P/1PP3q1/2KN1R2 w - - 0 27', 'R5f3', 'f5f3'],
                 ['2b4r/1r3pk1/1qp5/3pp1pP/3bP3/PpBP1P2/1P2Q1P1/RB1K3R b - - 0 28', 'Bxc3', 'd4c3'],
                 ['r1bq1rk1/ppp2p2/2np1n1p/2b1p1N1/2B1P2B/2NP4/PPP2PPP/R2Q1RK1 b - - 0 9', 'hxg5', 'h6g5']
                 ]

    for pos, mv, move_str in positions:
        assert str(Move(mv, Position(pos))) == move_str

# @pytest.mark.skip(reason="no way of currently testing this")
def test_invalid_target_square():
    posforrook = 'r2qkbnr/pppb1ppp/2npp3/8/4P3/5N2/PPPPBPPP/RNBQ1RK1 b kq - 0 5'
    p = Position(posforrook)
    m = Move('Rb9', Position(posforrook))
    #sprint('move', m)
    assert m.get_newposition(p, 'Ra8') == "Either source square or targetsquare not found"


def test_castle():
    posw = "r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R w KQkq - 0 1"
    posb = "r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R b KQkq - 0 1"
    kingside_castle = 'O-O'
    queenside_castle = 'O-O-O'
    assert str(Move(kingside_castle, Position(posw))) == 'e1g1'
    assert str(Move(queenside_castle, Position(posw))) == 'e1c1'
    assert str(Move(kingside_castle, Position(posb))) == 'e8g8'
    assert str(Move(queenside_castle, Position(posb))) == 'e8c8'
    