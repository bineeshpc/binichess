#!/usr/bin/python

# -*- coding: iso-8859-15 -*-
# generated by wxGlade 0.6.3 on Sun Feb 20 22:14:58 2011

import wx
import wx.grid
import string
import board
import sys
import re
from game import Game
from parser import Parser
from bclogger import logging
from multiprocessing import Pool
import time
import random
import string
import commands
import threading 

from twisted.internet import wxreactor
from twisted.internet import defer

wxreactor.install()

# import twisted reactor *only after* installing wxreactor
from twisted.internet import reactor, protocol
from twisted.protocols import basic


# begin wxGlade: extracode
# end wxGlade

initialpos ="""
       ---------------------------------
    8  | *R| *N| *B| *Q| *K| *B| *N| *R|     Move # : 1 (White)
       |---+---+---+---+---+---+---+---|
    7  | *P| *P| *P| *P| *P| *P| *P| *P|
       |---+---+---+---+---+---+---+---|
    6  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    5  |   |   |   |   |   |   |   |   |     Black Clock : 10:00
       |---+---+---+---+---+---+---+---|
    4  |   |   |   |   |   |   |   |   |     White Clock : 10:00
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    2  | P | P | P | P | P | P | P | P |     White Strength : 39
       |---+---+---+---+---+---+---+---|
    1  | R | N | B | Q | K | B | N | R |
       ---------------------------------
         a   b   c   d   e   f   g   h
"""

ID_FILE_OPEN = wx.NewId()
ID_FILE_CLOSE = wx.NewId()
ID_FILE_EXIT = wx.NewId()
ID_HELP_ABOUT = wx.NewId()
ID_ENGINE = wx.NewId()
ID_ABUSE = wx.NewId()

class MyFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: MyFrame.__init__
        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        #self.Bind(wx.EVT_CLOSE, self.OnClose)

        self.protocol = None  # twisted Protocol
        
        self.set_initialization()
        self.__set_properties()
        self.__do_layout()
        self.Bind(wx.EVT_SIZE, self.OnSize)
        # end wxGlade
        


    def set_initialization(self):
        self.boardsize = 8
        self.board =  board.Board()

        self.move = []
        self.position = initialpos
        self.buttons = []
        self.buttonwidth = 60
        self.buttonheight = 60
        self.connected = False
        self.game = Game()
        self.settings = { #'formula (standard) && (time>=10)': '',
                         'autoflag': 'on',
                         'noescape':'on',
                         'notakeback':'on'
                         }


    def __set_properties(self):
        # begin wxGlade: MyFrame.__set_properties
        self.SetTitle("BiniChess")
        self.CreateStatusBar()
        self.SetStatusText('Welcome to a BiniChess version 1.0')
        file_menu = wx.Menu()
        help_menu = wx.Menu()
        mode_menu = wx.Menu()
        abuse_menu = wx.Menu()
        file_menu.Append(ID_FILE_OPEN, 'Open File')
        file_menu.Append(ID_FILE_CLOSE, 'Close File')
        file_menu.AppendSeparator()
        file_menu.Append(ID_FILE_EXIT, 'Exit Program')
        abuse_menu.Append(ID_ABUSE, 'Abuse opponent')
        help_menu.Append(ID_HELP_ABOUT, 'About')
        self.enginemode = mode_menu.Append(ID_ENGINE, 'Engine', kind=wx.ITEM_CHECK)


        menu_bar = wx.MenuBar()
        menu_bar.Append(file_menu, 'File')
        menu_bar.Append(mode_menu, 'Mode')
        menu_bar.Append(abuse_menu, 'Abuse')
        menu_bar.Append(help_menu, 'Help')

        self.SetMenuBar(menu_bar)
        wx.EVT_MENU(self, ID_HELP_ABOUT, self.OnHelpAbout)

        wx.EVT_MENU(self, ID_ABUSE, self.OnAbuse)
        # end wxGlade


    def OnHelpAbout(self, evt):
        dlg = wx.MessageDialog(self, 'BiniChess is designed by Bini!', 'Development is still going on!',
                             wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()

    def OnAbuse(self, evt):
        cmd = "cd /home/bineesh/Dropbox/bin; python2 chat.py -U {} -n 1 -f bw.txt -m '{}'"
        message = ''.join([random.choice('!@#$%^&*()') for i in range(10)])
        player = self.game.get_opponent_name()
        cmd_st = cmd.format(player, message)
        def execute_cmd(cmd):
            logging.info(cmd)
            print(cmd)
            commands.getoutput(cmd)

        t = threading.Thread(target=execute_cmd, args=(cmd_st,))
        t.start()
        
        print("Abused {}".format(player))


    def getposition(self):
        """ Parses te position string and returns
        a list of length 64

        example:
           [   
' R ', '   ', '   ', ' K ', ' Q ', ' B ', ' N ', ' R ',
' P ', ' P ', ' P ', '   ', ' P ', ' P ', ' P ', ' P ',
'   ', '   ', '   ', '   ', '   ', '   ', '   ', '   ',
'   ', '   ', '   ', ' P ', ' N ', ' B ', '   ', '   ',
'   ', ' *P', '   ', '   ', '   ', '   ', '   ', '   ',
'   ', '   ', ' *P', '   ', '   ', ' *P', '   ', ' *P',
' *P', '   ', '   ', ' *P', ' *P', '   ', ' *P', '   ',
' *R', ' *N', ' *B', ' *K', ' *Q', ' *B', ' *N', ' *R'
]
        """
        brd = []
        for line in self.position.split("\n"):
            board = line[8:40]
            if board.find("|") != -1:
                for i in board.split("|"):
                    if i != '' and i.find('+') == -1:
                        brd.append(i)
        return brd



        
    def inttotuple(self, c):
        """ 
        (0, '->', ((0, 0), 0))
        (7, '->', ((7, 0), 7))
        (8, '->', ((0, 1), 1))
        (15, '->', ((7, 1), 8))
        (16, '->', ((0, 2), 2))
        (55, '->', ((7, 6), 13))
        (63, '->', ((7, 7), 14))
        """
        x = c % self.board.size
        y = c / self.board.size
        return (x, y), x + y


    def inttosquare(self, c):
        """(0, '->', 'a8')
        (7, '->', 'h8')
        (8, '->', 'a7')
        (15, '->', 'h7')
        (16, '->', 'a6')
        (55, '->', 'h2')
        (56, '->', 'a1')
        (62, '->', 'g1')
        (63, '->', 'h1')
        """
        x, y = self.inttotuple(c)[0]
        x = string.lowercase[x]
        y = self.board.size - y
        return str(x) + str(y)


    def squaretoint(self, c):
        """
        ('a8', '->', 0)
        ('h8', '->', 7)
        ('a7', '->', 8)
        ('h7', '->', 15)
        ('a6', '->', 16)
        ('h2', '->', 55)
        ('a1', '->', 56)
        ('g1', '->', 62)
        ('h1', '->', 63)
        """
        x, y = c[0], 8 - int(c[1])
        x =  string.lowercase.index(x)
        return x + y * 8


    def piece_to_Bitmap(self, piece, squarecolour, width, height):
        path = "../icons/"
        piececolour = ''
        piece = str(piece)
        if squarecolour is True:
            squarecolour = 'w'
        else:
            squarecolour = 'b'
        if piece.strip() == '':
            piece = ''
        elif piece[1] != '*':
            piece = piece.strip().lower()
            piececolour = 'w'
        else:
            piece = piece[2].strip().lower()
            piececolour = 'b'
        name = piececolour + piece + squarecolour + ".gif"
        def scale_bitmap(bitmap, width, height):
            image = wx.ImageFromBitmap(bitmap)
            #image = image.Scale(width, height, wx.IMAGE_QUALITY_HIGH)
            image = image.Scale(width, height)
            result = wx.BitmapFromImage(image)
            return result
        bitmap = wx.Bitmap(unicode(path + name), wx.BITMAP_TYPE_GIF)
        return scale_bitmap(bitmap, width, height)

    def __do_layout(self):
        # begin wxGlade: MyFrame.__do_layout
        bc_Frame_BoxSizer = wx.BoxSizer(wx.HORIZONTAL)
        bc_Frame_BoxSizer_GridSizer = wx.GridSizer(8, 8, 0, 0)

        # initializes 64 buttons which represents chessboard squares
        for cnt, piece in enumerate(self.getposition()):
            squarecolour = self.inttotuple(cnt)[1]  % 2 == 0
            bm = self.piece_to_Bitmap(piece, squarecolour, self.buttonwidth, self.buttonheight)
            bt = wx.BitmapButton(bitmap=bm,
              id=cnt, 
              parent=self, size=wx.Size(self.buttonwidth, self.buttonheight),
              style=wx.BU_EXACTFIT)
            bt.Bind(wx.EVT_BUTTON, self.OnButton)
            self.buttons.append(bt)
            bc_Frame_BoxSizer_GridSizer.Add(bt)
            
        bc_Frame_BoxSizer.Add(bc_Frame_BoxSizer_GridSizer, 1, 0, 1)
        self.textcontrol = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE, size=wx.Size(400, 400))
        self.commander = wx.TextCtrl(self, -1, "", style=wx.TE_PROCESS_ENTER)
        self.commander.Bind(wx.EVT_TEXT_ENTER, self.send_text)
        self.lastmove = wx.StaticText(self, -1, "LastMove Space")
        self.chessclock = wx.StaticText(self, -1, "Clock Space")
        self.timer = wx.Timer(self, -1)
        self.Bind(wx.EVT_TIMER, self.onTick, self.timer)
        self.timer.Start(milliseconds=1000, oneShot=False)
        print "Timer is ", self.timer.IsRunning()
        
        bc_Frame_BoxSizerright = wx.BoxSizer(wx.VERTICAL)
        bc_Frame_BoxSizerright.Add(self.textcontrol, 0, wx.EXPAND, 0)
        bc_Frame_BoxSizerright.Add(self.commander, 0, wx.EXPAND, 0)
        bc_Frame_BoxSizerright.Add(self.lastmove, 0, wx.EXPAND, 0)
        bc_Frame_BoxSizerright.Add(self.chessclock, 0, wx.EXPAND, 0)

        bc_Frame_BoxSizer.Add(bc_Frame_BoxSizerright)
        self.SetSizer(bc_Frame_BoxSizer)
        bc_Frame_BoxSizer.Fit(self)
        self.Layout()        
        self.Refresh()


    def onTick(self, evt):
        self.game.chessclock.tick()
        self.chessclock.SetLabel(self.game.chessclock.gettime())


    def login(self):
        guest_string = 'Guest' +''.join([random.choice(string.uppercase) for i in range(4)])
        self.playername = guest_string
        self.protocol.sendLine(guest_string)
        self.protocol.sendLine("\r\n")


    def configure_fics_settings(self):
        for k, v in self.settings.iteritems():
            setting = 'set %s %s' %(k, v)
            self.protocol.sendLine(setting)
            
        # end wxGlade
    def send_text(self, evt):
        print "in send"
        if self.connected is False:
            self.login()
            #self.protocol.sendLine("ob /b")
            self.configure_fics_settings()
            self.connected = True
        cmd = str(self.commander.GetValue())
        print "Sending the command %s" %cmd
        #for i in cmd: print ord(i)
        self.protocol.sendLine(cmd)
        self.commander.SetValue("")

    def autoplay(self):
        print("in autoplay")
        print("protocol is ", self.protocol)
        if self.connected is False:
            self.login()
            self.configure_fics_settings()
            self.connected = True
        #self.enginemode.Check()
        color_choices = ['black', 'white', '']
        color_choices = ['']
        color = random.choice(color_choices)

        #self.protocol.sendLine("seek 2 0 {}\r\n".format(color))
        #time.sleep(2)
        #self.protocol.sendLine("seek 2 1 {}\r\n".format(color))
        #time.sleep(2)
        #self.protocol.sendLine("seek 2 2 {}\r\n".format(color))



    def OnButton(self, event):
        id = event.GetId()
        square = self.inttosquare(id)
        def blacks_view(move):
            files = string.lowercase[:8] 
            file1 = move[0]
            rank1 = move[1]
            file2 = files[8 - files.index(file1) - 1]
            rank2 = 9 - int(rank1)
            return file2 + str(rank2)
        if self.positiondetails['sidetomove'] == 'Black':
            square = blacks_view(square)
        if len(self.move) == 1:
            if square != self.move[0]:
                self.move.append(square)
        else:
            self.move.append(square)
        if(len(self.move) == 2):
            move = ''.join(self.move)
            button_source = self.buttons[self.squaretoint(self.move[0])]
            button_dest = self.buttons[self.squaretoint(self.move[1])]

            #self.commander.AppendText("\n")
            self.textcontrol.AppendText("Sending the move %s" %move)
            self.protocol.sendLine(move)
            self.move = []
        self.Refresh()


    def OnSize(self, event):
        pass


class Handler:
    def __init__(self):
        self.parser = Parser()

    def is_bell(self, s):
        """ we typically get a bell when it is our move """
        return s == '\a'
 
    def handle_illegal_move(self, data, gui):
        if self.parser.is_pattern_match(data, 'illegal move'):
            print "Illegal move found"
            gui.move = []
    
    def handle_game_over(self, data, gui):
        if self.parser.is_pattern_match(data, 'game over'):
            if not ('Creating' in data):
                try:
                    print('Game over:')
                    logging.debug('Game over:')
                    game_result = self.parser.get_matched_pattern(data, 'game won')
                    if not game_result:
                        game_result = self.parser.get_matched_pattern(data, 'game drawn')
                    if game_result:
                        gui.game.write_game(game_result)
                    logging.debug("game result is : {}".format(game_result))
                    title_string = "{} {} {}-{}".format(game_result['player'],
                        game_result['message'],
                    game_result['whitepoint'],
                    game_result['blackpoint'])
                    logging.debug("title string is : {}".format(title_string))
                    gui.SetTitle(title_string)
                    last_move_message = "{}. {} {}".format(
                            gui.positiondetails['movenumber'],
                            gui.positiondetails['move'],
                            title_string)
                    logging.debug("Last move message is {} :".format(last_move_message))
                    gui.lastmove.SetLabel(last_move_message)
                    gui.timer.Stop()
                    gui.Refresh()
                    #gui.autoplay()
                except Exception as e:
                    print(e)

    def send_engine_move(self, gui, protocol):

        def send_move(en_move):
            
            logging.info("en move is {}".format(en_move))
            if en_move != '':
                if len(en_move) == 5:
                    en_move = en_move[:-1] + '=' + en_move[-1]
                    
                protocol.sendLine(en_move)
                print "sent",  en_move
                gui.game.bestmove = ''


        def print_success(en_move):
            msg = "Trying to send move {}".format(en_move)
            print(msg)
            logging.info(msg)

        def print_error(en_move):
            msg = "could not send move {}".format(en_move)
            print(msg)
            logging.info(msg)

        def get_engine_move_helper(deferred, gui):
            en_move = gui.game.get_engine_move(gui.positiondetails)
            logging.info("In engine move helper {}".format(en_move))
            deferred.callback(en_move)


        def get_engine_move(gui, protocol):
            deferred = defer.Deferred()
            reactor.callLater(.001, get_engine_move_helper, deferred, gui)
            #deferred.addCallback(print_success)
            deferred.addCallback(send_move)
            deferred.addErrback(print_error)
            logging.info("just before returning from get engine move")
            return deferred

        return get_engine_move(gui, protocol)
        #en_move = get_engine_move(gui, protocol)
        #send_move(en_move)
        
            
    def handle_move(self, data, gui, protocol):
        if self.is_bell(data[0]) or re.search(r'Creating', data):
            if gui.enginemode.IsChecked():
                gui.game.enginemode = True
            else:
                gui.game.enginemode = False

            gui.position = data
            
            position_details = self.parser.get_pattern_with_moves(gui.position)
            if position_details:
                gui.positiondetails = position_details
                print('matched pattern with moves')
                print(gui.positiondetails)
                lastmove = '{}. {}'.format(gui.positiondetails['movenumber'],
                gui.positiondetails['move'])
                gui.lastmove.SetLabel(lastmove)
            else:
                position_details = self.parser.get_pattern_without_moves(gui.position)
                if position_details:
                    gui.positiondetails = position_details
                    print("matched pattern without moves")
                    print(gui.positiondetails)
                    gui.timer.Start()
                    
                    gui.game.set_players(gui.positiondetails['white'], gui.positiondetails['black'], gui.playername)
            
                else:
                    print("did not match pattern without moves")
                    logging.debug('unparsed:')
                    logging.debug(data)
                    logging.debug('unparsed end:')
                    return

            logging.debug("gui position:")
            logging.debug(gui.getposition())
            logging.debug("position details :" + str(gui.positiondetails))

            for cnt, piece in enumerate(gui.getposition()):
                squarecolour = gui.inttotuple(cnt)[1]  % 2 == 0
                gui.buttons[cnt].SetBitmapLabel(gui.piece_to_Bitmap(piece, squarecolour, gui.buttonwidth, gui.buttonheight))
                

            gui.game.addficsposition(gui.getposition(), gui.positiondetails, data)
            
            try:
                playernames = '{} vs {}'.format(gui.game.whiteplayer, gui.game.blackplayer)
            except:
                playernames = 'Bini'

            if gui.game.mymove:
                gui.SetTitle("Your Move {}".format(playernames))
            else:
                gui.SetTitle("Opponent's Move {}".format(playernames))
            gui.Update()    
            gui.Refresh()
            print("is engine mode ", gui.game.enginemode)
            logging.debug('{} {} {} {}'.format(str(gui.game.playername), str(gui.game.is_my_move(gui.positiondetails)), gui.game.whiteplayer, gui.game.blackplayer))
            if gui.game.enginemode and gui.game.is_my_move(gui.positiondetails):
                print("inside the if of engine mode")
                self.send_engine_move(gui, protocol)
            gui.Refresh()
                
            gui.move = []
        
    def set_text_control(self, data, gui):
        data = data.decode('utf-8', 'ignore')
        gui.textcontrol.SetValue(data)
        gui.textcontrol.SetInsertionPointEnd()

    def handle(self, protocol, data):
        gui = protocol.factory.gui
        
        
        """
        logging.debug("seeking game")
        seek_games = self.parser.get_matched_pattern(data, 'seek game')
        logging.debug("seek games is " + str(seek_games))
        if seek_games:
            self.sendLine('play {}\r\n'.format(seek_games['gamenumber']))
        """
        
        if gui:
            gui.protocol = protocol
            if not gui.connected:
                gui.autoplay()
            val = gui.textcontrol.GetValue()
            print data.strip('\a') # print data without bell

            self.handle_illegal_move(data, gui)    
            self.handle_move(data, gui, protocol)
            self.handle_game_over(data, gui)
            self.set_text_control(data, gui)    
            

        
# end of class MyFrame

class DataForwardingProtocol(basic.LineReceiver):
    def __init__(self):
        self.output = None       
        self.addHandler(Handler())

    def addHandler(self, handler):
        self.handler = handler

    def dataReceived(self, data):
        print "data Received"
        logging.debug("data begin")
        logging.debug(data)
        logging.debug("data end")
        self.handler.handle(self, data)

    def connectionMade(self):
        self.output = self.factory.gui.textcontrol  # redirect Twisted's output
        

class ChessFactory(protocol.ClientFactory):
    def __init__(self, gui):
        self.gui = gui
        self.protocol = DataForwardingProtocol

    def clientConnectionLost(self, transport, reason):
        reactor.stop()

    def clientConnectionFailed(self, transport, reason):
        reactor.stop()


        
if __name__ == "__main__":
    app = wx.PySimpleApp(0)
    bc_Frame = MyFrame(None, -1, "")
    app.SetTopWindow(bc_Frame)
    bc_Frame.Show()
    reactor.registerWxApp(app)
    reactor.connectTCP("freechess.org", 5000, ChessFactory(bc_Frame))
    reactor.run()
