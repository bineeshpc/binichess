['   ', '   ', ' *K', ' *R', '   ', '   ', '   ', ' *R', ' *P', ' *P', ' *P', '   ', '   ', ' *P', ' *P', ' *P', '   ', ' *N', '   ', '   ', ' *P', '   ', '   ', '   ', '   ', '   ', ' *B', '   ', ' P ', '   ', '   ', '   ', '   ', '   ', '   ', ' *N', '   ', '   ', '   ', '   ', '   ', '   ', ' N ', '   ', '   ', ' P ', ' B ', '   ', ' P ', ' P ', '   ', '   ', ' B ', '   ', '   ', ' P ', '   ', '   ', ' K ', ' R ', '   ', '   ', '   ', ' R ']
{'movenumber': '18', 'sidetomove': 'Black', 'move': 'O', 'timeused': '0:09', 'whiteclock': '1:30', 'game': '194', 'black': 'Nusbaum', 'whitestrength': '24', 'blackclock': '2:23', 'white': 'Morgenstein', 'blackstrength': '26'}

Game 194 (Morgenstein vs. Nusbaum)

       ---------------------------------
    8  |   |   | *K| *R|   |   |   | *R|     Move # : 18 (Black)
       |---+---+---+---+---+---+---+---|
    7  | *P| *P| *P|   |   | *P| *P| *P|     White Moves : 'O-O-O   (0:09)'
       |---+---+---+---+---+---+---+---|
    6  |   | *N|   |   | *P|   |   |   |
       |---+---+---+---+---+---+---+---|
    5  |   |   | *B|   | P |   |   |   |     Black Clock : 2:23
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | *N|   |   |   |   |     White Clock : 1:30
       |---+---+---+---+---+---+---+---|
    3  |   |   | N |   |   | P | B |   |     Black Strength : 26
       |---+---+---+---+---+---+---+---|
    2  | P | P |   |   | B |   |   | P |     White Strength : 24
       |---+---+---+---+---+---+---+---|
    1  |   |   | K | R |   |   |   | R |
       ---------------------------------
         a   b   c   d   e   f   g   h
fics% 
