import logging
logging.basicConfig(
    filename='binichess.log',
    level=logging.DEBUG,
    format = '%(asctime)s %(levelname)s %(message)s')
