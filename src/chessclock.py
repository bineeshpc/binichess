class ChessClock:
    def __init__(self, wtime=2, btime=2, sidetomove='w'):
        self.wtime = wtime
        self.btime = btime
        self.sidetomove = sidetomove

    def tick(self):
        if self.sidetomove == 'w':
            self.wtime -= 1
        else:
            self.btime -= 1

    def settime(self, wtime, btime, sidetomove):
        self.wtime = self.toseconds(wtime)
        self.btime = self.toseconds(btime)
        self.sidetomove = sidetomove

    def toseconds(self, time):
        val = [int(i) for i in time.split(':')]
        val.reverse()
        return sum([60 ** i * j for i, j in enumerate(val)])

    def gettime(self):
        return "%s : %s" %(self.tostring(self.wtime), self.tostring(self.btime))
    
    def tostring(self, time):
        base = 60 * 60
        val = []
        if time <= 0:
            return "00:00:00"
        while base != 0:
            #print base, time % base, time / base
            val.append(time / base)
            time = time % base
            base = base / 60
        return ':'.join([str(i) for i in val])



