from chessclock import ChessClock

def test_chessclock_gettime():
    cc = ChessClock()
    cc.settime('1:11:39', '1:55:33', 'w')
    assert cc.gettime() == '1:11:39 : 1:55:33'

def test_chessclock_zero():
    cc = ChessClock()
    assert cc.tostring(0) == "00:00:00"
            
def test_chessclock_tick():
    cc = ChessClock()
    cc.settime('1:11:39', '1:55:33', 'w')
    cc.tick()
    assert cc.gettime() == '1:11:38 : 1:55:33'
    cc.tick()
    assert cc.gettime() == '1:11:37 : 1:55:33'
    cc.sidetomove = 'b'
    for i in range(3):
        cc.tick()
    assert cc.gettime() == '1:11:37 : 1:55:30'
