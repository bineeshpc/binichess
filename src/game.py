from bclogger import logging
import re
from position import Position
from move import Move
import os
from engine import Engine
from chessclock import ChessClock
import datetime
import time
import random

samplepgn = """
[Event "ICS unrated blitz match"]
[Site "freechess.org"]
[Date "2007.06.15"]
[Round "-"]
[White "GuestRKGX"]
[Black "guestHIHI"]
[Result "1-0"]
[WhiteElo "0"]
[BlackElo "0"]
[TimeControl "120+12"]

1. e4 e6 2. d4 d5 3. exd5 exd5 4. Nc3 Nf6 5. Bg5 Be7 6. Qd3 O-O 7. O-O-O
Nc6 8. Re1 Be6 9. h4 a5 10. h5 Nd7 11. Bxe7 Qxe7 12. Nf3 Nb4 13. Qd2 c6 14.
h6 g6 15. Ng5 Nb6 16. Nxe6 fxe6 17. g3 Qf6 18. Re2 a4 19. Bh3 Nc4 20. Bxe6+
Qxe6 21. Qe1 Qf6 22. Re7 b5 23. Rg7+ Kh8 24. b3 Nd6 25. a3 Na6 26. bxa4
bxa4 27. Nxa4 Rae8 28. Qa5 Re7 29. Rxe7 Qxe7 30. Qxa6 Qb7 31. Qe2 Ne4 32.
f3 Nxg3 33. Qe5+ Kg8 34. Qxg3 Qb5 35. Nc3
{guestHIHI resigns} 1-0


"""


class Game:
    def __init__(self, pgn=''):
        self.tags = {}
        self.positions = []
        self.ficspositions = []
        self.uci = []
        self.engine = Engine()
        self.bestmove = ''
        self.enginemode = False
        self.mymove = False
        self.whiteplayer = None
        self.blackplayer = None
        self.playername = None
        self.game_result = None
        self.chessclock = ChessClock()
        self.date = datetime.date.today().strftime('%Y.%m.%d')

        tag_regex = re.compile(r"\[(?P<name>.*?) \"(?P<value>.*)\"")
        
        for line in pgn.split("\n"):
            m = re.match(r"\[.*\]", line)
            if m is not None:
                tag = m.group()
                logging.debug(tag)
                tagdict = tag_regex.match(tag).groupdict()
                logging.debug(tagdict)
                v = tagdict.values()
                self.tags[v[0]] = v[1]
                continue

        m = re.search(r"1\.[ \w\.\n=+#{}-]*", pgn)
        if m is not None:
            self.notation = m.group()

        if pgn is not '':
            self.pgnread()


    def set_players(self, whiteplayer, blackplayer, playername):
        self.whiteplayer = whiteplayer
        self.blackplayer = blackplayer
        self.playername = playername
                        
    def is_my_move(self, positiondetails):
        try:
            #position = self.ficspositions[-1]
            sidetomove = positiondetails['sidetomove']
            if sidetomove == 'Black' and self.is_player_black():
                return True
            if sidetomove == 'White' and self.is_player_white():
                return True
            return False
        except:
            print("fics pos missing in is my move")
            logging.debug("fics pos missing in is my move")
            return False
                
    def get_opponent_name(self):
        if self.is_player_white():
            return self.blackplayer
        else:
            return self.whiteplayer
            
    def is_player_white(self):
        return self.playername == self.whiteplayer

    def is_player_black(self):
        return self.playername == self.blackplayer
    
    def get_player_color(self):
        if self.is_player_white():
            return 'white'
        else:
            return 'black'

    def get_opponent_color(self):
        if self.get_player_color() == 'white':
            return 'black'
        else:
            return 'white'

    def get_time_remaining(self, positiondetails):
        time_white = positiondetails['whiteclock'].split(':')
        timeremaining_white = (int(time_white[0]) * 60 + int(time_white[1])) * 1000
        time_black = positiondetails['blackclock'].split(':')
        timeremaining_black = (int(time_black[0]) * 60 + int(time_black[1])) * 1000
        return timeremaining_white, timeremaining_black
        
    def get_player_timeremaining(self, positiondetails):
        white_time, black_time = self.get_time_remaining(positiondetails)
        if self.get_player_color() == 'white':
            return white_time
        else:
            return black_time

    def get_opponent_timeremaining(self, positiondetails):
        white_time, black_time = self.get_time_remaining(positiondetails)
        if self.get_player_color() == 'white':
            return black_time
        else:
            return white_time


    def addficsposition(self, position='', details='', data=''):
        
        # print "Inside the game"
        # print position, details, data
        if data != '':
            
            if re.search(r" *\d *\|", data).group().strip()[0] == '1':
                view = 'black'
                
            else:
                view = 'white'

            pos = Position(fics=position, view=view)
            pos.sidetomove = details['sidetomove'][0].lower()
            self.chessclock.settime(details['whiteclock'], details['blackclock'], pos.sidetomove)
            #pos.display()
            try:
                hm = details['move']

            except KeyError:
                print "Starting position without moves found"

                self.uci = []
                self.ficspositions = []
                #self.ficspositions.append(Position())
            else:
                #print hm
                try:
                    oldpos = self.ficspositions[len(self.ficspositions) -1]
                    
                except IndexError:
                    print "Starting position while observing a game found"
                    self.ficspositions = []
                    mv = Move(hm, Position())
                    print mv
                    self.uci.append(mv)
                    
                else:
                    #oldpos.display()
                    #print self.ficspositions
                    mv = Move(hm, oldpos)
                    #print mv
                    logging.info('Move being appended to uci is {}'.format(str(mv)))
                    self.uci.append(mv)
                    #print self.uci

                    time_f = details[view + 'clock'].split(':')
                    timeremaining = (int(time_f[0]) * 60 + int(time_f[1])) * 1000
                    print "clock is :", timeremaining
                    
                    player_timeremaining = self.get_player_timeremaining(details)
                    opponent_timeremaining = self.get_opponent_timeremaining(details)

                    if pos.sidetomove == view[0]:
                        self.mymove = True
                        remaining_time_ratio = float(player_timeremaining) / opponent_timeremaining
                        if remaining_time_ratio < .5:
                            self.enginemode = True
                    else:
                        self.mymove = False
                        
                #print view
                self.ficspositions.append(pos)
                #print pos.display()
                #print self.ficspositions

    def get_engine_move(self, position_details):
        
        timeremaining = self.get_player_timeremaining(position_details)
        # uci engines expects the promotion piece to be represented in lowercase
        move_strings = [str(i).lower() for i in self.uci]
        moves_eng = ' '.join(move_strings)
        position_eng = "position startpos moves %s\ngo movetime %d\n" %(moves_eng, timeremaining / 60)
        mv_eng = None
        self.engine.getmove(position_eng)

        while True:
            if not mv_eng:
                mv_eng = self.engine.bestmove
                break
            time.sleep(.01)
        self.bestmove = mv_eng
        logging.info("Engine move is {}".format(self.bestmove))
        return mv_eng
        
    def pgnread(self):
        self.positions.append(Position())
        self.uci = []
        for fullmove in re.split(r"\d*\.", self.notation):
            if fullmove.strip() == '': continue
            for el in fullmove.split():
                m = re.match(r"[a-hKQNBR][a-h1-8KQNBR=x#+]*|O-O-O|O-O", el)
                if m is not None:
                    hm = m.group()
                    print hm
                    position = self.positions[len(self.positions) -1]
                    mv = Move(hm, position)
                    print mv
                    self.uci.append(mv)
                    self.positions.append(mv.newposition)

        for num, move in enumerate(self.uci):
            fm = num / 2
            if not num % 2:
                print "%s. %s" %(fm + 1, move),
            else:
                print move

    def write_game(self, game_result, file='mygames.txt'):
        self.game_result = game_result
        mygames = open('mygames.txt', 'a')
        print self.uci
        mygames.write('[Event "{}"]\n'.format("ICS unrated blitz match"))
        mygames.write('[Date "{}"]\n'.format(self.date))
        mygames.write('[White "{}"]\n'.format(self.whiteplayer))
        mygames.write('[Black "{}"]\n'.format(self.blackplayer))
        result = "{}-{}".format(self.game_result['whitepoint'], self.game_result['blackpoint'])
        mygames.write('[Result "{}"]\n'.format(result))
        try:
            print self.ficspositions[0]
        except:
            pass
        for k , v in enumerate(self.uci):
            if k % 2 == 0:
                mygames.write("%s. %s" %(k / 2 + 1, v))
            else:
                mygames.write(" %s " %v)
        mygames.write("\n")
        if game_result['whitepoint'] != '1/2':
            mygames.write(" {%s %s}" % (self.game_result['player'], self.game_result['message']))
        else:
            mygames.write(" {%s}" % (self.game_result['message']))
        mygames.write(" {}".format(result))
        mygames.write("\n")
        mygames.close()
                        

    
