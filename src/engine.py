import re
import subprocess
import sys
import random
import glob
import time
import threading

#engines = glob.glob('*.exe')

class Engine(object):
    def __init__(self):
        # self.engines = ["stockfish", "fruit", "toga2"]
        self.engines = ["stockfish", 
        'stockfish_10_x64_modern',
         'stockfish_10_x64',
          'stockfish_10_x64_bmi2']

        self.engine = random.choice(self.engines)
        self.start_engine()
        self.bestmove = None

    def start_engine(self):
        self.process = subprocess.Popen(
            self.engine,
            shell=False,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE)
        print "Started the engine %s" % self.engine

    def getmove(self, position):
        self.set_bestmove(None)
        get_move_thread = threading.Thread(target=self.send_to_engine, args=(position,))
        get_move_thread.start()
        get_move_thread.join()

    def send_to_engine(self, position):
        self.process.stdin.write(position)
        print "Getmove %s" %position
        while True:
            output = self.process.stdout.readline()
            #sys.stdout.write('Received: %s' %output)
            #sys.stdout.flush()
            if(output.find('bestmove') != -1):
                bestmove = output.split()[1]
                self.set_bestmove(bestmove)
                break
            time.sleep(.001)


    def set_bestmove(self, move):
        self.bestmove = move


    def __del__(self):
        self.process.terminate()

