from position import Position

def test_position_from_fen():
    p = Position("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1")
    expected = """
(a8=r)(b8=n)(c8=b)(d8=q)(e8=k)(f8=b)(g8=n)(h8=r)
(a7=p)(b7=p)(c7=p)(d7=p)(e7=p)(f7=p)(g7=p)(h7=p)
(a6=)(b6=)(c6=)(d6=)(e6=)(f6=)(g6=)(h6=)
(a5=)(b5=)(c5=)(d5=)(e5=)(f5=)(g5=)(h5=)
(a4=)(b4=)(c4=)(d4=)(e4=P)(f4=)(g4=)(h4=)
(a3=)(b3=)(c3=)(d3=)(e3=)(f3=)(g3=)(h3=)
(a2=P)(b2=P)(c2=P)(d2=P)(e2=)(f2=P)(g2=P)(h2=P)
(a1=R)(b1=N)(c1=B)(d1=Q)(e1=K)(f1=B)(g1=N)(h1=R)
"""
    assert p.display() == expected
    
    assert p.castlingavailability == 'KQkq '
    
def test_get_non_existant_attribute():
    p = Position("rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1")
    assert p.nonexistant == None


def get_position(position):
    brd = []
    for line in position.split("\n"):
        board = line[8:40]
        if board.find("|") != -1:
            for i in board.split("|"):
                if i != '' and i.find('+') == -1:
                    brd.append(i)
    return brd


def test_position_fics_position_black_view():

    startpos = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
    fics_pos = """Game 44 (GuestPYCF vs. GuestDRVG)

       ---------------------------------
    1  | R | N | B | K | Q | B | N | R |     Move # : 1 (White)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P | P | P | P | P | P |
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   |   |   |   |   |   |     Black Clock : 3:00
       |---+---+---+---+---+---+---+---|
    5  |   |   |   |   |   |   |   |   |     White Clock : 3:00
       |---+---+---+---+---+---+---+---|
    6  |   |   |   |   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P| *P| *P| *P| *P| *P| *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% 

"""
    expected = """
(a8=r)(b8=n)(c8=b)(d8=q)(e8=k)(f8=b)(g8=n)(h8=r)
(a7=p)(b7=p)(c7=p)(d7=p)(e7=p)(f7=p)(g7=p)(h7=p)
(a6=)(b6=)(c6=)(d6=)(e6=)(f6=)(g6=)(h6=)
(a5=)(b5=)(c5=)(d5=)(e5=)(f5=)(g5=)(h5=)
(a4=)(b4=)(c4=)(d4=)(e4=)(f4=)(g4=)(h4=)
(a3=)(b3=)(c3=)(d3=)(e3=)(f3=)(g3=)(h3=)
(a2=P)(b2=P)(c2=P)(d2=P)(e2=P)(f2=P)(g2=P)(h2=P)
(a1=R)(b1=N)(c1=B)(d1=Q)(e1=K)(f1=B)(g1=N)(h1=R)
"""
    p = Position(startpos, fics=get_position(fics_pos), view='black')
    assert p.display() == expected


def test_position_fics_position_white_view():

    startpos = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
    fics_pos = """
Game 39 (GuestZRXR vs. GuestGGNN)

       ---------------------------------
    8  | *R| *N| *B| *Q| *K| *B| *N| *R|     Move # : 1 (White)
       |---+---+---+---+---+---+---+---|
    7  | *P| *P| *P| *P| *P| *P| *P| *P|
       |---+---+---+---+---+---+---+---|
    6  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    5  |   |   |   |   |   |   |   |   |     Black Clock : 30:00
       |---+---+---+---+---+---+---+---|
    4  |   |   |   |   |   |   |   |   |     White Clock : 30:00
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    2  | P | P | P | P | P | P | P | P |     White Strength : 39
       |---+---+---+---+---+---+---+---|
    1  | R | N | B | Q | K | B | N | R |
       ---------------------------------
         a   b   c   d   e   f   g   h

Game 39: A disconnection will be considered a forfeit.
"""
    expected = """
(a8=r)(b8=n)(c8=b)(d8=q)(e8=k)(f8=b)(g8=n)(h8=r)
(a7=p)(b7=p)(c7=p)(d7=p)(e7=p)(f7=p)(g7=p)(h7=p)
(a6=)(b6=)(c6=)(d6=)(e6=)(f6=)(g6=)(h6=)
(a5=)(b5=)(c5=)(d5=)(e5=)(f5=)(g5=)(h5=)
(a4=)(b4=)(c4=)(d4=)(e4=)(f4=)(g4=)(h4=)
(a3=)(b3=)(c3=)(d3=)(e3=)(f3=)(g3=)(h3=)
(a2=P)(b2=P)(c2=P)(d2=P)(e2=P)(f2=P)(g2=P)(h2=P)
(a1=R)(b1=N)(c1=B)(d1=Q)(e1=K)(f1=B)(g1=N)(h1=R)
"""
    p = Position(startpos, fics=get_position(fics_pos), view='white')

    assert p.display() == expected

