from game import Game, samplepgn

def test():
    g = Game(samplepgn)
    g.addficsposition()
    g.write_game(file='/tmp/mygames.txt')
    #g = Game()

def test_enginemove():
    fools_mate = """[Event "ICS unrated blitz match"]
[Site "freechess.org"]
[Date "2007.06.15"]
[Round "-"]
[White "GuestRKGX"]
[Black "guestHIHI"]
[Result "1-0"]
[WhiteElo "0"]
[BlackElo "0"]
[TimeControl "120+12"]

1. g4 e6 2. f3


    """
    g = Game(fools_mate)
    assert g.get_engine_move({u'movenumber': '2', u'sidetomove': 'Black', u'move': 'f3', u'timeused': '0:00', u'whiteclock': '1:00', u'game': '45', u'black': 'GuestKBDN', u'whitestrength': '39', u'blackclock': '1:00', u'white': 'GuestHZFF', u'blackstrength': '39'}) == 'd8h4'


def test_setplayers():
    simple_pgn = """[Event "ICS unrated blitz match"]
[Site "freechess.org"]
[Date "2007.06.15"]
[Round "-"]
[White "GuestRKGX"]
[Black "guestHIHI"]
[Result "1-0"]
[WhiteElo "0"]
[BlackElo "0"]
[TimeControl "120+12"]

1. g4 e6 2. f3


    """
    g = Game(simple_pgn)
    g.set_players('GuestWhite', 'GuestBlack', 'GuestWhite')
    position_details = {u'movenumber': '2', u'sidetomove': 'Black', u'move': 'f3', u'timeused': '0:00', u'whiteclock': '1:00', u'game': '45', u'black': 'GuestKBDN', u'whitestrength': '39', u'blackclock': '1:00', u'white': 'GuestHZFF', u'blackstrength': '39'}
    assert g.is_player_white() == True
    assert g.is_player_black() == False
    assert g.is_my_move(position_details) == False


def test_add_fics_position():
    simple_pgn = """[Event "ICS unrated blitz match"]
[Site "freechess.org"]
[Date "2007.06.15"]
[Round "-"]
[White "GuestRKGX"]
[Black "guestHIHI"]
[Result "1-0"]
[WhiteElo "0"]
[BlackElo "0"]
[TimeControl "120+12"]

1. g4 e6 2. f3


    """
    g = Game(simple_pgn)
    g.addficsposition()