import string

class Colour:
    """ Represents the color of a square
    """
    (white, black) = (0, 1)
    
class Board:
    """
       the board abstraction. This is apart from the UI
    """
    def __init__(self):
    
        self.size = 8
        self.numsquares = self.size ** 2
        self.squares = [(i, j) for i in range(1, self.size + 1) for j in range(1, self.size + 1)]
        self.perspective = Colour.white  # do you see the board from white's or blacks perspective
    
          
        
    def colour(self, square):
        """
         Returns true if square is black
        """
        if isinstance(square, tuple):
            file, rank = square[0], square[1]
            file = string.lowercase.index(file) + 1
            return not (file + rank) % 2

    def display(self):
        print(self.squares)
        
    def squarenames(self):
        """
         Gives the square names in form 
         ('a', 1), ('c', 8) etc
        """
        for square in self.squares:
            file, rank = square
            rank, file = file, rank
            file = string.lowercase[file - 1]
            yield file, self.size - rank + 1
