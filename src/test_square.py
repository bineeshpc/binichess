from square import Square, InvalidSquare
import pytest

def test_squarenum():
    for  i in range(64):
        assert Square(i).squarenum == i

def test_squarestring():
    square_str = 'a8'
    assert str(Square(square_str)) == square_str

def test_invalidsquare():
    with pytest.raises(InvalidSquare):
        Square([]) # send and invalid type list as argument

def test_square_in_different_forms():
    x, y = 0, 0
    square = 'a1'
    squarenum = 0
    sq = Square((x, y))
    assert sq.square == square
    assert sq.coordinates == (x, y)
    assert sq.squarenum == 0

def test_place_piece():
    sq = Square((1, 2))
    piece = 'b'
    sq.addpiece(piece)
    assert sq.content == piece