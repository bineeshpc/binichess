import re
import sys
from square import Square
from bclogger import logging

startpos = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

class Position(object):
    def __init__(self, fen=startpos, fics='', view=''):
        """ Position can be added using fen or fics
        This creates a position  from fen or fics
        """

        if fics != '':
            self.position = []
            if view == 'black':
                cnt = 0
                for i in range(8):
                    for j in range((i + 1) * 8 - 1, i * 8 - 1, -1):
                        content = fics[j].strip()
                        try:
                            if content[0] == '*':
                                piece = content[1].lower()
                            else: piece = content
                        except IndexError:
                            piece = ''
                        sq = Square(cnt)
                        sq.addpiece(piece)
                        #print sq
                        self.position.append(sq)
                        cnt += 1
            else:
                cnt = 0
                for i in range(7, -1, -1):
                    for j in range(8):
                        ind = i * 8 + j
                        content = fics[ind].strip()
                        try:
                            if content[0] == '*':
                                piece = content[1].lower()
                            else: piece = content
                        except IndexError:
                            piece = ''
                            
                        sq = Square(cnt)
                        sq.addpiece(piece)
                        #print sq
                        self.position.append(sq)
                        cnt += 1
                 
                 
                        
        else:                        

            pattern = r"""(?P<pos>.*?)\ (?P<sidetomove>.?) (?P<castlingavailability>.*?\ )(?P<enpassenttarget>.*?\ )(?P<halfmoveclock>.*?\ )(?P<fullmovenumber>.*)"""
            self.matchdict = re.match(pattern, fen).groupdict()
            logging.debug(self.matchdict)
            self.position = []
            cnt = 0
            for row in reversed(self.pos.split("/")):
                logging.debug(row)
                for value in row:
                    try:
                        if int(value):
                            for i in range(int(value)):
                                sq = Square(cnt)
                                cnt += 1
                                sq.addpiece('')
                                self.position.append(sq)
                    except ValueError:
                        sq = Square(cnt)
                        cnt += 1
                        sq.addpiece(value)
                        self.position.append(sq)


    def display(self):
        position_list = []
        for i in range(7, -1, -1):
            position_list.append('\n')
            for j in range(8):
                cnt = i * 8 + j
                sq = self.position[cnt]
                position_list.append("(%s=%s)" %(sq.square, sq.content))
                #print sq.square, sq.content, sq.coordinates, sq.squarenum,
##        for sq in self.position:
##            print sq.square, sq.content, sq.coordinates, sq.squarenum
        position_list.append('\n')
        position_string = ''.join(position_list)
        print(position_string)
        return position_string

    def __getattr__(self, name):
        try:
            return self.__getattribute__(name)
        except AttributeError:
            try:
                return self.matchdict[name]  #get attribute from confdict
            except KeyError:
                print "Value %s not found" %name


        
