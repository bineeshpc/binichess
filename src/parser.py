from bclogger import logging
import re
import json

class Parser:
    def __init__(self):
        
        self.patterns = json.loads(open('settings.json').read())
        self.patterns_compiled = {}
        for k, (v1, v2) in self.patterns.items():
            if v2 is False:
                self.patterns_compiled[k] = re.compile(v1)
            elif v2 == True:
                self.patterns_compiled[k] = re.compile(v1, re.DOTALL)
            elif v2 == 'multiline':
                self.patterns_compiled[k] = re.compile(v1, re.MULTILINE)
        


    def is_pattern_match(self, data, pattern_str):
        if self.patterns_compiled[pattern_str].search(data):
            return True
        return False

    def get_pattern_with_moves(self, data):
        return self.get_matched_pattern(data, 'pattern with moves')

    def get_pattern_without_moves(self, data):
        return self.get_matched_pattern(data, 'pattern without moves')


    def get_matched_pattern(self, data, pattern_str):

        out = self.patterns_compiled[pattern_str].search(data)
        if out:
            return out.groupdict()
        else:
            return None


