from bclogger import logging

class InvalidSquare(Exception):
    pass

num = 8
class Square:
    def __init__(self, arg):
        
        if isinstance(arg, str):          
            self.square = arg
            
        elif isinstance(arg, int):
            file = arg % num
            file = chr(ord('a') + file)
            rank = arg / num + 1
            self.square = str(file) + str(rank)

        elif isinstance(arg, tuple):
            file, rank = arg
            file = chr(ord('a') + file)
            rank += 1
            self.square = str(file) + str(rank)
        else:
            raise InvalidSquare("Accessed with %s" %arg)
        
        self.coordinates = self.getcoordinates()
        self.squarenum = self.getsquarenum()

    def addpiece(self, piece):
        self.content = piece

    def getcoordinates(self):
        file, rank = self.square[0], self.square[1]
        return ord(file) - ord('a'), int(rank) - 1

    def getsquarenum(self):
        file, rank = self.coordinates
        return file + rank * 8
        
    def __str__(self):
        return self.square


