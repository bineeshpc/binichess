"""
Cannot test the original protocol because of import issues
test a copy pasted one instead
This would give me an idea which code should move away from GUI
"""

from parser import Parser
from bclogger import logging
from game import Game
from board import Board
import re
import time

class Textcontrol:
    def GetValue(self):
        pass

    def SetValue(self, data):
        pass


    def SetInsertionPointEnd(self):
        pass

class Engine:
    def __init__(self):
        self.checked = False

    def IsChecked(self):
        return self.checked

class Button:
    def __init__(self):
        pass

    def SetBitmapLabel(self, str_1):
        pass

class MoveLabel:
    def __init__(self):
        pass

    def SetLabel(self, str_1):
        pass


class GUI:
    def __init__(self):
        self.connected = False
        self.textcontrol = Textcontrol()
        self.enginemode = Engine()
        self.game = Game()
        self.playername = 'GuestAEYZ'
        self.board = Board()
        self.buttons = [Button() for i in range(64)]
        self.buttonwidth = 1
        self.buttonheight = 1
        self.lastmove = MoveLabel()

    def autoplay(self):
        pass

    def SetTitle(self, title):
        self.title = title

    def Refresh(self):
        pass
    
    def getposition(self):
        brd = []
        for line in self.position.split("\n"):
            board = line[8:40]
            if board.find("|") != -1:
                for i in board.split("|"):
                    if i != '' and i.find('+') == -1:
                        brd.append(i)
        return brd


    def inttotuple(self, c):
        x = c % self.board.size
        y = c / self.board.size
        return (x, y), x + y

    def piece_to_Bitmap(self, piece, squarecolour, width, height):
        path = "../icons/"
        piececolour = ''
        piece = str(piece)
        if squarecolour is True:
            squarecolour = 'w'
        else:
            squarecolour = 'b'
        if piece.strip() == '':
            piece = ''
        elif piece[1] != '*':
            piece = piece.strip().lower()
            piececolour = 'w'
        else:
            piece = piece[2].strip().lower()
            piececolour = 'b'
        name = piececolour + piece + squarecolour + ".gif"
        return name

class Factory:
    def __init__(self, gui=GUI()):
        self.gui = gui

class DataForwardingProtocol:
    def __init__(self):
        self.output = None
        self.used = False
        self.parser = Parser()
        self.factory = Factory()

    def dataReceived(self, data):
        print "data Received"
        logging.debug("data begin")
        logging.debug(data)
        logging.debug("data end")
        gui = self.factory.gui
        gui.protocol = self
        
        logging.debug("seeking game")
        seek_games = self.parser.get_matched_pattern(data, 'seek game')
        logging.debug("seek games is " + str(seek_games))
        if seek_games:
            self.sendLine('play {}\r\n'.format(seek_games['gamenumber']))

        if not gui.connected:
            gui.autoplay()

        if gui:
            val = gui.textcontrol.GetValue()
            print data.strip('\a')
            
            
            if self.parser.is_pattern_match(data, 'illegal move'):
                print "Illegal move found"
                gui.move = []
            
            if self.parser.is_pattern_match(data, 'game over'):
                print('Game over:')
                if not ('Creating' in data):
                    gui.game.write_game()
            
            if ord(data[0]) == 7 or re.search(r'Creating', data):
                if gui.enginemode.IsChecked():
                    gui.game.enginemode = True
                    while self.used is True:
                        time.sleep(.2)
                        self.used = False
                else:
                    gui.game.enginemode = False
                    
                self.used = True
                gui.position = data
                
                position_details = self.parser.get_pattern_with_moves(gui.position)
                if position_details:
                    gui.positiondetails = position_details
                    print('matched pattern with moves')
                    print(gui.positiondetails)
                    gui.lastmove.SetLabel(gui.positiondetails['move'])
                else:
                    position_details = self.parser.get_pattern_without_moves(gui.position)
                    if position_details:
                        gui.positiondetails = position_details
                        print("matched pattern without moves")
                        print(gui.positiondetails)
                        if not gui.game.whiteplayer:
                            gui.game.set_players(gui.positiondetails['white'], gui.positiondetails['black'], gui.playername)
                
                    else:
                        print("did not match pattern without moves")
                        logging.debug('unparsed:')
                        logging.debug(data)
                        return

                logging.debug("gui position:")
                logging.debug(gui.getposition())
                logging.debug("position details :" + str(gui.positiondetails))

                for cnt, piece in enumerate(gui.getposition()):
                    squarecolour = gui.inttotuple(cnt)[1]  % 2 == 0
                    gui.buttons[cnt].SetBitmapLabel(gui.piece_to_Bitmap(piece, squarecolour, gui.buttonwidth, gui.buttonheight))
                    

                gui.game.addficsposition(gui.getposition(), gui.positiondetails, data)

                if gui.game.mymove:
                    gui.SetTitle("Your Move Bini")
                    gui.Refresh()
                else:
                    gui.SetTitle("Opponent's Move Bini")
                    gui.Refresh()
                
                print("is engine mode ", gui.game.enginemode)
                if gui.game.enginemode and gui.game.is_my_move(gui.positiondetails):
                    print("inside the if of engine mode")
                    gui.Refresh()
                    time.sleep(.2)
                    en_move = gui.game.get_engine_move(gui.positiondetails)
                    print("en move is ", en_move)
                    if en_move != '':
                        if len(en_move) == 5:
                            en_move = en_move[:-1] + '=' + en_move[-1]
                            
                        self.sendLine(en_move)
                        print "sent",  en_move
                        gui.game.bestmove = ''
                gui.move = []
            
            data = data.decode('utf-8', 'ignore')
            gui.textcontrol.SetValue(data)
            gui.textcontrol.SetInsertionPointEnd()
            self.used = False

    def sendLine(self, data):
        print(data)

    def connectionMade(self):
        self.output = self.factory.gui.textcontrol  # redirect Twisted's output


class SimulatedServer:
    def __init__(self):
        self.datas = [
            'GuestFBQR (++++) seeking 5 5 unrated blitz ("play 11" to respond)',
            """Creating: GuestFBQR (++++) GuestAEYZ (++++) unrated blitz 5 5
{Game 63 (GuestFBQR vs. GuestAEYZ) Creating unrated blitz match.}

Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R | N | B | K | Q | B | N | R |     Move # : 1 (White)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P | P | P | P | P | P |
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   |   |   |   |   |   |     Black Clock : 5:00
       |---+---+---+---+---+---+---+---|
    5  |   |   |   |   |   |   |   |   |     White Clock : 5:00
       |---+---+---+---+---+---+---+---|
    6  |   |   |   |   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P| *P| *P| *P| *P| *P| *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a

Game 63: A disconnection will be considered a forfeit.
fics% fics% 
            """,
            """ 
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R | N | B | K | Q | B | N | R |     Move # : 1 (Black)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     White Moves : 'e4      (0:00)'
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P |   |   |   |   |     Black Clock : 5:00
       |---+---+---+---+---+---+---+---|
    5  |   |   |   |   |   |   |   |   |     White Clock : 5:00
       |---+---+---+---+---+---+---+---|
    6  |   |   |   |   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P| *P| *P| *P| *P| *P| *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% """,
"""
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R | N | B | K | Q | B | N | R |     Move # : 2 (White)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     Black Moves : 'g5      (0:00)'
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P |   |   |   |   |     Black Clock : 5:00
       |---+---+---+---+---+---+---+---|
    5  |   | *P|   |   |   |   |   |   |     White Clock : 5:00
       |---+---+---+---+---+---+---+---|
    6  |   |   |   |   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P|   | *P| *P| *P| *P| *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% """,
"""
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R |   | B | K | Q | B | N | R |     Move # : 2 (Black)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     White Moves : 'Nf3     (0:02)'
       |---+---+---+---+---+---+---+---|
    3  |   |   | N |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P |   |   |   |   |     Black Clock : 5:00
       |---+---+---+---+---+---+---+---|
    5  |   | *P|   |   |   |   |   |   |     White Clock : 5:03
       |---+---+---+---+---+---+---+---|
    6  |   |   |   |   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P|   | *P| *P| *P| *P| *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% """,
"""
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R |   | B | K | Q | B | N | R |     Move # : 3 (White)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     Black Moves : 'f6      (0:09)'
       |---+---+---+---+---+---+---+---|
    3  |   |   | N |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P |   |   |   |   |     Black Clock : 4:56
       |---+---+---+---+---+---+---+---|
    5  |   | *P|   |   |   |   |   |   |     White Clock : 5:03
       |---+---+---+---+---+---+---+---|
    6  |   |   | *P|   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P|   |   | *P| *P| *P| *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics%""", """
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R |   | B | K | Q | B | N | R |     Move # : 3 (Black)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     White Moves : 'Nd4     (0:18)'
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P | N |   |   |   |     Black Clock : 4:56
       |---+---+---+---+---+---+---+---|
    5  |   | *P|   |   |   |   |   |   |     White Clock : 4:50
       |---+---+---+---+---+---+---+---|
    6  |   |   | *P|   |   |   |   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P|   |   | *P| *P| *P| *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% """,
"""
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R |   | B | K | Q | B | N | R |     Move # : 4 (White)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     Black Moves : 'c6      (0:18)'
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P | N |   |   |   |     Black Clock : 4:43
       |---+---+---+---+---+---+---+---|
    5  |   | *P|   |   |   |   |   |   |     White Clock : 4:50
       |---+---+---+---+---+---+---+---|
    6  |   |   | *P|   |   | *P|   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P|   |   | *P| *P|   | *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% """,
"""
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R |   |   | K | Q | B | N | R |     Move # : 4 (Black)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     White Moves : 'Bc4     (0:04)'
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P | N | B |   |   |     Black Clock : 4:43
       |---+---+---+---+---+---+---+---|
    5  |   | *P|   |   |   |   |   |   |     White Clock : 4:51
       |---+---+---+---+---+---+---+---|
    6  |   |   | *P|   |   | *P|   |   |     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P|   |   | *P| *P|   | *P| *P|     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% """,
"""
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R |   |   | K | Q | B | N | R |     Move # : 5 (White)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     Black Moves : 'a6      (0:09)'
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P | N | B |   |   |     Black Clock : 4:39
       |---+---+---+---+---+---+---+---|
    5  |   | *P|   |   |   |   |   |   |     White Clock : 4:51
       |---+---+---+---+---+---+---+---|
    6  |   |   | *P|   |   | *P|   | *P|     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P|   |   | *P| *P|   | *P|   |     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% """,
"""
Game 63 (GuestFBQR vs. GuestAEYZ)

       ---------------------------------
    1  | R |   |   | K |   | B | N | R |     Move # : 5 (Black)
       |---+---+---+---+---+---+---+---|
    2  | P | P | P |   | P | P | P | P |     White Moves : 'Qh5#    (0:03)'
       |---+---+---+---+---+---+---+---|
    3  |   |   |   |   |   |   |   |   |
       |---+---+---+---+---+---+---+---|
    4  |   |   |   | P | N | B |   |   |     Black Clock : 4:39
       |---+---+---+---+---+---+---+---|
    5  | Q | *P|   |   |   |   |   |   |     White Clock : 4:53
       |---+---+---+---+---+---+---+---|
    6  |   |   | *P|   |   | *P|   | *P|     Black Strength : 39
       |---+---+---+---+---+---+---+---|
    7  | *P|   |   | *P| *P|   | *P|   |     White Strength : 39
       |---+---+---+---+---+---+---+---|
    8  | *R| *N| *B| *K| *Q| *B| *N| *R|
       ---------------------------------
         h   g   f   e   d   c   b   a
fics% 
{Game 63 (GuestFBQR vs. GuestAEYZ) GuestAEYZ checkmated} 1-0

No ratings adjustment done.
fics% """ 
        ]

def test():
    d = DataForwardingProtocol()
    s = SimulatedServer()
    for data in s.datas:
        d.dataReceived(data)